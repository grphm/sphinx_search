<?php
\Route::group(['prefix' => 'search', 'middleware' => 'public'], function() {

    $this->post('/', ['as' => 'application.search.global', 'uses' => 'Controller@search']);
    $this->post('/more', ['as' => 'application.search.more', 'uses' => 'Controller@searchMore']);
});