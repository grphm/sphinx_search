<?php
namespace APPLICATION_HOME\Http\Controllers;

use Illuminate\Support\Collection;
use STALKER_CMS\Packages\Sphinx\SphinxClient;

class Controller extends ModuleController {

    private $search_total = 0;

    public function search() {

        return redirect()->to(route('public.page.search_result').'?search='.\Request::input('request'));
    }

    public function SearchGlobal() {

        $request = \RequestController::init();
        $search_result = new Collection();
        if(\ValidatorController::passes($request, ['search' => 'required'])):
            if($pages_blocks = $this->SphinxSearchPageBlocks($request::input('search'))):
                $pages = [];
                foreach($pages_blocks as $index => $item):
                    if($item->page->locale == \App::getLocale()):
                        $pages[$item->page->slug] = ['slug' => $item->page->slug, 'title' => $item->page->title, 'content' => $item->content, 'published_start' => NULL];
                    endif;
                endforeach;
                foreach($pages as $page):
                    $search_result->push($page);
                endforeach;
            endif;
        endif;
        return [$search_result, $this->search_total];
    }

    public function searchMore() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['amount' => 'required|numeric', 'from' => 'required|numeric'])):
            $html = FALSE;
            $search_results = new Collection();
            if($pages_blocks = $this->SphinxSearchPageBlocks($request::input('search'), $request::input('from'))):
                $pages = [];
                foreach($pages_blocks as $index => $item):
                    if($item->page->locale == \App::getLocale()):
                        $pages[$item->page->slug] = ['slug' => $item->page->slug, 'title' => $item->page->title, 'content' => $item->content, 'published_start' => NULL];
                    endif;
                endforeach;
                foreach($pages as $page):
                    $search_results->push($page);
                endforeach;
            endif;
            if($search_results->count()):
                $html = view('site_views::assets.search', compact('search_results'))->render();
            endif;
            return \ResponseController::success(200)->set('html', $html)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    private function SphinxSearchPageBlocks($searchText, $offset = 0) {

        $sphinx = new SphinxSearch();
        $result = $sphinx->search($searchText, 'PagesBlockIndex')
            ->setFieldWeights(['content' => 10])
            ->setMatchMode(SphinxClient::SPH_MATCH_ANY)
            ->setRankingMode(SphinxClient::SPH_RANK_PROXIMITY_BM25)
            ->SetSortMode(SphinxClient::SPH_SORT_RELEVANCE, "@weight DESC")
            ->limit(5, $offset)->with('page')->get();
        $this->search_total += $sphinx->search($searchText, 'PagesBlockIndex')->getTotalCount();
        if(!empty($result) && !isset($result['total'])):
            foreach($result as $index => $item):
                $result[$index]->content = multiSpace(strip_tags($item->content));
            endforeach;
            $excerpts = $sphinx->excerpts(array_pluck($result, 'content'), ['before_match' => '<strong>', 'after_match' => '</strong>', 'chunk_separator' => '', 'around' => 10]);
            foreach($result as $index => $item):
                $result[$index]->content = @$excerpts[$index];
            endforeach;
            return $result;
        else:
            return NULL;
        endif;
    }
}


#########################################################################
# SELECT content_pages_blocks.id, content_pages_blocks.content, content_pages.title \
# FROM content_pages_blocks inner join content_pages on content_pages_blocks.page_id = content_pages.id
#########################################################################