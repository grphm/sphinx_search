<?php
namespace APPLICATION_HOME\Http\Controllers;

use STALKER_CMS\Packages\Sphinx\SphinxClient;

class SphinxSearch extends ModuleController {

    protected $_connection;
    protected $_index_name;
    protected $_search_string;
    protected $_config;
    protected $_total_count;
    protected $_time;
    protected $_eager_loads;

    public function __construct() {

        $host = config('application::config.sphinx.host');
        $port = config('application::config.sphinx.port');
        $timeout = config('application::config.sphinx.timeout');
        $this->_connection = new SphinxClient();
        $this->_connection->setServer($host, $port);
        $this->_connection->setConnectTimeout($timeout);
        $this->_connection->setMatchMode(SphinxClient::SPH_MATCH_ANY);
        $this->_connection->setSortMode(SphinxClient::SPH_SORT_RELEVANCE);
        $this->_config = config('application::config.sphinx.indexes');
        reset($this->_config);
        $this->_index_name = isset($this->_config['name']) ? implode(',', $this->_config['name']) : key($this->_config);
        $this->_eager_loads = [];
    }

    public function search($string, $index_name = NULL) {

        $this->_search_string = $string;
        if(NULL !== $index_name):
            if(strpos($index_name, ' ') || strpos($index_name, ',')):
                if(!isset($this->_config['mapping'])):
                    $this->_config['mapping'] = FALSE;
                endif;
            endif;
            $this->_index_name = $index_name;
        endif;
        $this->_connection->resetFilters();
        $this->_connection->resetGroupBy();
        return $this;
    }

    public function setFieldWeights($weights) {

        $this->_connection->setFieldWeights($weights);
        return $this;
    }

    public function setMatchMode($mode) {

        $this->_connection->setMatchMode($mode);
        return $this;
    }

    public function setRankingMode($mode) {

        $this->_connection->setRankingMode($mode);
        return $this;
    }

    public function setSortMode($mode, $sortby = NULL) {

        $this->_connection->setSortMode($mode, $sortby);
        return $this;
    }

    public function setFilterFloatRange($attribute, $min, $max, $exclude = FALSE) {

        $this->_connection->setFilterFloatRange($attribute, $min, $max, $exclude);
        return $this;
    }

    public function setGeoAnchor($attrlat, $attrlong, $lat = NULL, $long = FALSE) {

        $this->_connection->setGeoAnchor($attrlat, $attrlong, $lat, $long);
        return $this;
    }

    public function setGroupBy($attribute, $func, $groupsort = '@group desc') {

        $this->_connection->setGroupBy($attribute, $func, $groupsort);
        return $this;
    }

    public function setSelect($select) {

        $this->_connection->setSelect($select);
        return $this;
    }

    public function limit($limit, $offset = 0, $max_matches = 1000, $cutoff = 1000) {

        $this->_connection->setLimits($offset, $limit, $max_matches, $cutoff);
        return $this;
    }

    public function filter($attribute, $values, $exclude = FALSE) {

        if(is_array($values)):
            $val = array();
            foreach($values as $v):
                $val[] = (int)$v;
            endforeach;
        else:
            $val = array((int)$values);
        endif;
        $this->_connection->setFilter($attribute, $val, $exclude);
        return $this;
    }

    public function range($attribute, $min, $max, $exclude = FALSE) {

        $this->_connection->setFilterRange($attribute, $min, $max, $exclude);
        return $this;
    }

    public function query() {

        return $this->_connection->query($this->_search_string, $this->_index_name);
    }

    public function excerpt($content, $opts = []) {

        return $this->_connection->buildExcerpts([$content], $this->_index_name, $this->_search_string, $opts);
    }

    public function excerpts($contents, $opts = []) {

        return $this->_connection->buildExcerpts($contents, $this->_index_name, $this->_search_string, $opts);
    }

    public function get($respect_sort_order = FALSE) {

        $this->_total_count = 0;
        $result = $this->_connection->query($this->_search_string, $this->_index_name);
        // Process results.
        if($result):
            // Get total count of existing results.
            $this->_total_count = (int)$result['total_found'];
            // Get time taken for search.
            $this->_time = $result['time'];
            if($result['total'] && isset($result['matches'])):
                // Get results' id's and query the database.
                $matchids = array_keys($result['matches']);
                $idString = implode(',', $matchids);
                $config = isset($this->_config['mapping']) ? $this->_config['mapping'] : $this->_config[$this->_index_name];
                if($config):
                    if(isset($config['modelname'])):
                        if($this->_eager_loads):
                            $result = call_user_func_array($config['modelname']."::whereIn", [$config['column'], $matchids])
                                ->orderByRaw(\DB::raw("FIELD(id, $idString)"))
                                ->with($this->_eager_loads)->get();
                        else:
                            $result = call_user_func_array($config['modelname']."::whereIn", [$config['column'], $matchids])
                                ->orderByRaw(\DB::raw("FIELD(id, $idString)"))
                                ->get();
                        endif;
                    else:
                        $result = \DB::table($config['table'])->whereIn($config['column'], $matchids)
                            ->orderByRaw(\DB::raw("FIELD(id, $idString)"))->get();
                    endif;
                endif;
            endif;
        else:
            $result = [];
        endif;
        if($respect_sort_order):
            if(isset($matchids)):
                $return_val = [];
                foreach($matchids as $matchid):
                    $key = self::getResultKeyByID($matchid, $result);
                    if(FALSE !== $key):
                        $return_val[] = $result[$key];
                    endif;
                endforeach;
                return $return_val;
            endif;
        endif;
        $this->_eager_loads = [];
        return $result;
    }

    public function with() {

        if(FALSE === isset($this->_eager_loads)):
            $this->_eager_loads = [];
        endif;
        foreach(func_get_args() as $a):
            if(is_array($a)):
                $this->_eager_loads = array_merge($this->_eager_loads, $a);
            else:
                $this->_eager_loads[] = $a;
            endif;
        endforeach;
        return $this;
    }

    public function getTotalCount() {

        return $this->_total_count;
    }

    public function getTime() {

        return $this->_time;
    }

    public function getErrorMessage() {

        return $this->_connection->getLastError();
    }

    private function getResultKeyByID($id, $result) {

        if(count($result) > 0):
            foreach($result as $k => $result_item):
                if($result_item->id == $id):
                    return $k;
                endif;
            endforeach;
        endif;
        return FALSE;
    }
}