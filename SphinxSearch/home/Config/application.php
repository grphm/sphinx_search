<?php

return [
    'sphinx' => [
        'host' => '127.0.0.1',
        'port' => 3312,
        'timeout' => 30,
        'indexes' => [
            'PagesBlockIndex' => [
                'table' => 'content_pages_blocks',
                'column' => 'id',
                'modelname' => 'STALKER_CMS\Core\Content\Models\PageBlock'
            ]
        ]
    ]
];